google.charts.load("current", {packages:["timeline"]});
google.charts.setOnLoadCallback(drawChart);
/* JS dates have a 0-based month, keep the data readable */
function dd(y, m, d) { return new Date(y, m-1, d); }
/* Swap columns so we can write an aligned table in the data */
function sc(title, start, end, name) { return [title, name, start, end]; }
function drawChart() {
  var container = document.getElementById('boardtimeline');
  var chart = new google.visualization.Timeline(container);
  var dataTable = new google.visualization.DataTable();

  dataTable.addColumn({ type: 'string', id: 'Term' });
  dataTable.addColumn({ type: 'string', id: 'Name' });
  dataTable.addColumn({ type: 'date', id: 'Start' });
  dataTable.addColumn({ type: 'date', id: 'End' });

  let currentDate = new Date();
  let currentYear = currentDate.getFullYear();
  let currentMonth = (currentDate.getMonth() + 1).toString().padStart(2,0);
  let currentDay = currentDate.getDate().toString().padStart(2,0);
  var CURRENT = dd(currentYear, currentMonth, currentDay);

  var president = 'President'
      , treasurer = 'Vice President & Treasurer'
      , vp = 'Vice President'
      , boardA = 'Board Member A'
      , boardB = 'Board Member B'
  ;
  dataTable.addRows([
    sc( president,  dd(1997, 11, 26), dd(1999, 10, 09), 'Matthias Ettrich' ),
    sc( president,  dd(1999, 10, 09), dd(2002, 08, 25), 'Kurt Granroth' ),
    sc( president,  dd(2002, 08, 25), dd(2005, 08, 26), 'Kalle Dalheimer' ),
    sc( president,  dd(2005, 08, 26), dd(2007, 11, 04), 'Eva Brucherseifer' ),
    sc( president,  dd(2007, 11, 04), dd(2009, 07, 07), 'Aaron Seigo' ),
    sc( president,  dd(2009, 07, 07), dd(2014, 08, 22), 'Cornelius Schumacher' ),
    sc( president,  dd(2014, 08, 22), dd(2019, 09, 09), 'Lydia Pintscher' ),
    sc( president,  dd(2019, 09, 09), CURRENT         , 'Aleix Pol i Gonzàlez' ),
    sc( treasurer,  dd(1997, 11, 25), dd(1999, 10, 09), 'Kalle Dalheimer' ),
    sc( treasurer,  dd(1999, 10, 09), dd(2005, 08, 26), 'Mirko Böhm' ),
    sc( treasurer,  dd(2005, 08, 26), dd(2009, 07, 07), 'Cornelius Schumacher' ),
    sc( treasurer,  dd(2009, 07, 07), dd(2012, 07, 03), 'Frank Karlitschek' ),
    sc( treasurer,  dd(2012, 07, 03), dd(2014, 08, 22), 'Agustín Benito Bethencourt' ),
    sc( treasurer,  dd(2014, 08, 22), dd(2017, 07, 22), 'Marta Rybczynska' ),
    sc( treasurer,  dd(2017, 07, 22), CURRENT         , 'Eike Hein' ),
    sc( vp,         dd(1997, 11, 25), dd(1999, 10, 09), 'Martin Konold' ),
    sc( vp,         dd(1999, 10, 09), dd(2002, 08, 25), 'Chris Schläger' ),
    sc( vp,         dd(2002, 08, 25), dd(2005, 08, 26), 'Eva Brucherseifer' ),
    sc( vp,         dd(2005, 08, 26), dd(2006, 09, 25), 'Mirko Böhm' ),
    sc( vp,         dd(2006, 09, 25), dd(2011, 08, 09), 'Adriaan de Groot' ),
    sc( vp,         dd(2011, 08, 09), dd(2013, 07, 12), 'Sebastian Kügler' ),
    sc( vp,         dd(2013, 07, 12), dd(2014, 08, 22), 'Lydia Pintscher' ),
    sc( vp,         dd(2014, 08, 22), dd(2019, 09, 09), 'Aleix Pol i Gonzàlez' ),
    sc( vp,         dd(2019, 09, 09), CURRENT         , 'Lydia Pintscher' ),
    sc( boardA,     dd(1997, 11, 25), dd(1999, 10, 09), 'Michael Renner' ),
    sc( boardA,     dd(1999, 10, 09), dd(2002, 08, 25), 'Preston Brown' ),
    sc( boardA,     dd(2002, 08, 25), dd(2004, 08, 20), 'Marie Loise Nolden' ),
    sc( boardA,     dd(2004, 08, 20), dd(2005, 08, 26), 'Harry Porten' ),
    sc( boardA,     dd(2005, 08, 26), dd(2007, 11, 04), 'Aaron Seigo' ),
    sc( boardA,     dd(2007, 11, 04), dd(2011, 08, 09), 'Sebastian Kügler' ),
    sc( boardA,     dd(2011, 08, 09), dd(2013, 07, 12), 'Lydia Pintscher' ),
    sc( boardA,     dd(2013, 07, 12), dd(2016, 09, 01), 'Albert Astals Cid' ),
    sc( boardA,     dd(2016, 09, 01), dd(2019, 09, 09), 'Thomas Pfeiffer' ),
    sc( boardA,     dd(2019, 09, 09), dd(2022, 10, 03), 'Neofytos Kolokotronis' ),
    sc( boardA,     dd(2022, 10, 03), CURRENT         , 'Nate Graham' ),
    sc( boardB,     dd(1997, 11, 25), dd(2007, 11, 03), '(added in 2007)' ),
    sc( boardB,     dd(2007, 11, 04), dd(2009, 07, 07), 'Klaas Freitag' ),
    sc( boardB,     dd(2009, 07, 07), dd(2012, 07, 03), 'Celeste Lyn Paul' ),
    sc( boardB,     dd(2012, 07, 03), dd(2015, 07, 24), 'Pradeepto Bhattacharya' ),
    sc( boardB,     dd(2015, 07, 24), dd(2018, 08, 13), 'Sandro Andrade' ),
    sc( boardB,     dd(2018, 08, 13), dd(2019, 09, 09), 'Andy Betts' ),
    sc( boardB,     dd(2019, 09, 09), CURRENT         , 'Adriaan de Groot' ),
  ]);

  var options = {
      width: 1000
  };
  chart.draw(dataTable, options);
}
