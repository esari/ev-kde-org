<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Events/LatinoWare/latinowaresmo.jpg" alt="Members of the KDE Brasil Network pose behind a LatinoWare sign." />
    </figure>
</div>

From the 2nd to the 4th of November, I traveled from São Paulo to Foz do Iguaçu, Paraná, to tend the KDE booth at the biggest free software event in Latin America, [Latinoware ](https://latinoware.org/)2022. I joined up with two great people who have been attending Latinoware for many years: Pedro and Barbara, and who have been contributing to KDE longer than I have.

### Day One

By the time I arrived, the booth was open. The booth consisted of a small space of about 2 by 4 meters. At the back, we had a blue KDE banner, a glass table and three chairs, and at the front, a booth counter where dozens of stickers, bookmarkers and keychains were being distributed while Barbara and Pedro were presenting KDE to the attendees.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Events/LatinoWare/folkswithstickers.jpg" alt="Two happy attendees, a girl and a boy, hold up KDE stickers and keychains." />
    </figure>
</div>

Initially, I assisted Pedro with distributing 3D-printed key-chains. Barbara was focusing on presenting Krita and Kdenlive, which are her specialities as a content creator and journalist. Pedro was focusing on community-oriented matters and presenting our groups and social media, especially KDE Brazil's Telegram channel, the most popular group we have in Brazil.

I brought my laptop to showcase things but I still needed to finish tweaking it. I also needed a proper Internet connection. When Barbara went to lunch, I got the chance to be the main speaker for a while. Barbara’s gaming laptop was used to showcase her work and the animation she made in Kdenlive for the event. Pedro used his laptop to showcase KDE Connect as well as other programs.

In the afternoon, things got hectic at the booth. People were loving the 3D-printed key-chains. Did I mention the key-chains, stickers and bookmarkers were all made by Barbara at her print shop using exclusively free software?

At a certain point, without noticing, I started talking nonstop at the booth. First a quick mention of what KDE (the community) is, and that we provide both a graphical desktop environment – I’d use the swipe gesture to hide all windows to say, "I mean this beautiful thing you can see over here. That is for Linux and is called Plasma. But we also provide a collection of applications" and I’d use the menu to unhide all windows and show a busy overview. "Most of them are cross-platform and run on Windows, Linux, Mac, Android, and even on Linux for phones!".

### Day Two

On the second day, I went to the booth earlier and took a walk around the surroundings. Our booth was next to the Linux Professional Institute booth, and saw Jon "Maddog" Hall with a Santa hat. A few booths to the side was the Debian booth. To our right and towards the back there was 3D-printing booths.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Events/LatinoWare/maddog.jpg" alt="Jon 'Maddog' Hall shows off portrait of... Jon 'Maddog' Hall made with Krita." />
    </figure>
</div>

Back at the KDE booth, I had prepared eight virtual desktops, each to showcase one or two programs at a time. On the first I had Firefox, with links to the Krita, Kdenlive, Plasma Mobile, and KDE websites. One the next desktop I had opened KDE's file managers, Dolphin and Index. Another showcased two popular text editros: Ghostwriter and Kate. The Okular PDF viewer came up next, and then Elisa, a music player. On another desktop I had a video player and streaming app called Haruna, and then KDE's Subtitle Composer. Finally I showcased the Discover app manager, and Plasma's System Settings app.

At Latinoware, the booths played a supplementary role to the main show, the talks. Barbara herself had two talking slots at the event: "How a print shop can work with free software", and "3D Printing in schools". On both occasions, while Barbara was away, Pedro and I tended the booth.

Three talks were of particular interest to KDE: "How to live without Photoshop" and "Showing how to use free software for graphics design", both by Elias Silveira; and "Videography with free software", by Carlos Eduardo Cruz, aka Cadunico.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Events/LatinoWare/konqidrawing.jpg" alt="Drawing a Konqi on a painting tablet using Krita." />
    </figure>
</div>

### Day Three

The third day was the slowest, but also the day I found most tiring. I was starting to feel myself getting voiceless from so much speaking in the few days I had been there. Despite that, I was still enjoying being the main speaker.

Fairly early on in the day, the keychains ran out.

I feel like I should mention how much stuff we had to distribute. The list of things we brought to give away to the people visiting our booth was as follows:

<figure class="image-right">
    <img width="100%" src="images/Events/LatinoWare/danishfolk.jpg" alt="Group of four people posing in the KDE booth. The second from the left is an exceptionally tall Danish youngster." />
</figure>

* 30 Konqi and Katie sticker sets (for kids)
* 120 KDE logo stickers
* 90 Konqi stickers with a white background
* 90 Konqi stickers with a blue background
* 140 Kdenlive stickers
* 324 white vinyl KDE logo stickers
* 324 blue vinyl KDE logo stickers
* 200 KDE 3D keychains
* 800 KDE bookmarkers

We distributed everything entirely for free, and everything was entirely made with free software.

Despite the fact that each person would grab multiple stickers and bookmarkers together, we can safely say that this three-day venture was a huge success. Several hundred people attended our booth, the majority of which would leave knowing more about KDE and the software we provide. They also took away with them bookmarkers printed with links to our Brazilian KDE website.

Based on my perception of people’s reactions, they loved everything, and many attendees were unaware that actual free software existed for educational purposes. The idea that KDE provided an entire suite of educational software was like icing on the cake.

### Results

A huge success:

* We distributed over 1100 stickers, 800 bookmarks, and 200 keychains.
* Visiting artists painted 3 pictures in front of people using Krita.
* We made [a video for KDE’s official YouTube/PeerTube channel](https://tube.kockatoo.org/w/k8eKWoFFhviWatciZcdHux) with the footage we filmed at the event.
