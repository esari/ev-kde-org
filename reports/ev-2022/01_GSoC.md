<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/GSoC/gsoc_logo.jpg" alt="Google Summer of Code banner logo" />
    </figure>
</div>


[Google Summer of Code (GSoC)](https://summerofcode.withgoogle.com/) is a global, online event that focuses on bringing new contributors into open source software development. Like every year, in 2022 KDE applied with the aim of integrating more and more developers. KDE's participation in GSoC covered nine projects to improve KDE, of which six were successfully completed.

<figure class="image-right"> <img width="100%" src="images/GSoC/Spaces_NeoChat.png" alt="Screenshot of NeoChat showing Spaces." /> </figure>

Snehit Sah worked on [adding Spaces Support](https://community.kde.org/GSoC/2022/StatusReports/SnehitSah) to [NeoChat](https://apps.kde.org/neochat/). *Spaces* is a Matrix tool that allows you to discover new rooms by exploring areas, and it is also a way to organize your rooms by categories.

<figure class="image-left"> <img width="100%" src="images/GSoC/Flatpak_Permissions.png" alt="Screenshot of showing Flatpak permissions." /> </figure>

Suhaas Joshi worked [on permission management for Flatpak and Snap applications in Discover](https://community.kde.org/GSoC/2022/StatusReports/SuhaasJoshi). This allows you to change the permissions granted to an application (e.g. to tweak access to the file system, network, and so on) and also makes it easier to review them. The code is in two separate repositories, [one for Flatpak applications which is ready to be used](https://community.kde.org/GSoC/2022/StatusReports/Samarthraj) , and one for Snap applications which was still work in progress at the time of the end of GSoC2022.

There were two projects to improve [digiKam](https://www.digikam.org/). The first one was from Quoc Hung Tran who worked on [a new plugin to process Optical Character Recognition (OCR)](https://community.kde.org/GSoC/2022/StatusReports/QuocHungTran). The code allows you to extract text from images and store the output inside the EXIF data or within a separate file. The plugin is also used to organize scanned text images with contents.

The second project is from Phuoc Khanh LE who worked on [improving the Image Quality Sorter algorithms](https://community.kde.org/GSoc/2022/StatusReports/PhuocKhanhLe). The code improves sorting images by quality using multiple criteria, for example, noise, exposure and compression.

<figure class="image-left"> <img width="100%" src="images/GSoC/GCompris_activity_mockup.png" alt="A mock up of a Gcompris adding activity." /> </figure>

For [GCompris](https://www.gcompris.net/), KDE's educational software suite, Samarth Raj worked on adding activities for [using the 10's complements to add numbers](https://community.kde.org/GSoC/2022/StatusReports/Samarthraj). Is split into three activities. One was finished during GSoC, and the other two were still work in progress byt the time the event finished.

Two students worked on the painting application [Krita](https://krita.org/). Xu Che worked on [adding pixel-perfect ellipses in Krita](https://community.kde.org/GSoC/2022/StatusReports/XuChe). The work was still in progress when GSoC finished, but it would allow pixel artists to use Krita more effectively.

Meanwhile, Reinold Rojas worked on [exporting Krita images to SVG](https://community.kde.org/GSoC/2022/StatusReports/ReinoldRojas). The project provided more options to share files with Inkscape, and helps create translatable images with text for Krita Manual without knowledge of Inkscape.

GSoC again provided our nine contributors with the opportunity to apply their knowledge of programming to real-world projects, allowing them to improve their code and communication skills.
