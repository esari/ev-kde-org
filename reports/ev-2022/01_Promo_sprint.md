<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Sprints/Promo/GroupPhoto.jpg" alt="The Promo team during downtime." />
    </figure>
</div>

On the 20th and 21st of August, KDE's Promo team held a two-day Sprint in Saumur, France. We were fortunate to attend along with Allon, Aron, Neophytos, Claudio, Paul, and Aniqa to contribute and discussed a range of topics. To give a taste, here we will focus on two of the topics discussed: a *KDE-for* promotional campaign and diversity initiatives at KDE.

<figure class="image-right">
    <img width="100%" src="images/Sprints/Promo/Planning_KDE-for_website.jpg" alt="Board showing drafts of ideas for the layout of a 'for' page." />
    <br/>
    <figcaption>Brainstorm for the layout of a 'for' page.</figcaption>
</figure>


*KDE-for* is an idea that existed before the Sprint. A *KDE-for-kids* website even exists, but we wanted to see how far we could take the idea. Our brainstorming generated numerous proposals, including *KDE-for-creators*, *KDE-for-developers*, *KDE-for-researchers*, *KDE-for-teachers*, *KDE-for-students*, *KDE-for-gamers*, *KDE-for-activists*, and so on. Each potential *for*\-group allows us to connect KDE products with end users and highlight the many communities within and around KDE. It also provides a place to showcase flagship apps while raising awareness about the sometimes lesser-known ones complimenting them. You can already take a look at the wip prototype [here](https://carlschwan.eu/2022/09/19/promo-sprint-in-saumur/api.carlschwan.eu/for/creators/) and if you have suggestions and want to help we have a [phabricator task](https://phabricator.kde.org/T15770).

One of the issues with the idea is what to do when there are software gaps. Should *KDE-for* also promote non-KDE software when it fulfils a useful but missing function for a particular target group? KDE software is excellent bar none, but we may not offer everything to meet every group's needs. User autonomy and choice within a rich software ecosystem are one of the undeniable strengths of FOSS. Although attempting a complete list of software options would not be helpful, why not support other FOSS developers and give users a curated list of excellent FOSS applications that may be useful to them? We are all on the same team after all!

Another topic which came up was how to promote diversity within KDE. As they say: One cannot expect diversity just to happen, one needs to cultivate it.

Over the past few years, KDE Promo has been pushing a [KDE network initiative](https://community.kde.org/The_KDE_Network) to build international communities. At the moment the networks include India, China, Brazil, and USA. One discussion at the Sprint centered around creating an additional Europe network -- or Europe: Spain, Europe: Germany, etc. networks -- to remove any implication of a default network. KDE is a worldwide community and it only makes sense that our internal and external structures reflect that. Another discussion was about expanding these networks, for instance, to Africa and Singapore and South Korea, among others. Perhaps you, the reader, can help build these networks where you live.

The *KDE-for* campaign and the expansion of KDE Networks will help communities make KDE software the best software it can be and we are proud to be a part of it.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Sprints/Promo/cooking.jpg" alt="Emmanuel cuts dough to be cooked into delicious buns in a wooden oven in his garden." />
        <br />
        <figcaption>Cooking at Emmanuel's house in the Loire countryside.</figcaption>
    </figure>
</div>

And, of course, in France we ate -- but not only that, we cooked! Our local host invited us to his garden with family and friends and made us Fouée in a wood-fired oven. All in all the Sprint was a great opportunity to push many important topics forward while having a wonderful time together. We look forward to the next one!
