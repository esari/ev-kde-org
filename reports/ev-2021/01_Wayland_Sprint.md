From the 23rd to the 24th of January, the Wayland team held their [annual sprint](https://community.kde.org/Sprints/Wayland/2021Virtual). During the sprint, the team discussed and worked on:

* The QtWayland threaded polling patch
* Scene Redesign
* Input Methods
* Virtual Keyboards
* Compositing Scheduling Recap
* Compositor hand-offs
* KWin Stand-alone/Embedded
* Wayland as Default
* Custom Plasma Protocols
* Feature parity
* QtWayland patches and Qt 5
* Wayland on Qt6 and KF6
