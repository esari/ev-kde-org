The fifteenth edition of [Akademy-es](https://www.kde-espana.org/akademy-es-2020), KDE's yearly event for the Spanish-speaking community, was held online from 20th to 22nd November. The event was packed with virtual talks, presentations, workshops and remote social events. We discovered and discussed the news of the KDE Community and the progress of one of the most important free projects within the Free Software ecosystem.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;"><a href="images/Akademy-es/Group_photo.png"><img src="images/Akademy-es/Group_photo.png" width="100%" /></a><br /><figcaption>Akademy-es group photo.</figcaption></figure>
</div>

The conference started with the opening ceremony by Adrián Chaves, president of [KDE Spain](https://www.kde-espana.org/). During the event, there were talks on various topics such as software licensing, podcasts with free software, [Plasma](https://kde.org/plasma-desktop/) and [Wayland](https://wayland.freedesktop.org/), [Flatpak](https://flatpak.org/), [Flathub](https://flathub.org/home) and KDE, and the latest news in KDE development, as well as lightning talks.

Talks addressed topics both for users and developers, and the event also included practical workshops and social activities.

Akademy-es helps connect KDE developers from all over Spain, so they can talk about the projects they are working on, share code, experiences and knowledge. It also contributes to promoting KDE projects to a wider audience.

Despite not being able to meet in person, Akademy-es, as always, managed to help us all enjoy learning more about Free Software and KDE.
