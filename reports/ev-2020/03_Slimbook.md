<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;"><a href="images/Products/kdeslimbook3.jpg"><img src="images/Products/kdeslimbook3.jpg" width="100%" /></a></figure>
</div>

In July 2020, [Slimbook](https://slimbook.es/) and [KDE](https://kde.org/) launched the new version of its now classic [KDE Slimbook](https://kde.slimbook.es/). The third generation of this popular ultrabook came in a stylish sleek magnesium alloy case less than 20 mms thick, but packed under the hood a powerful AMD Ryzen 7 4800 H processor with 8 cores and 16 threads. On top of that, it ran [KDE's Plasma desktop](https://kde.org/plasma-desktop/), complete with a wide range of preinstalled, ready-to-use Open Source utilities and apps.

Both things combined make the KDE Slimbook a one-of-a-kind machine ready for casual, everyday use, gaming and entertainment; design work, animation, and 3D rendering; as well as hardcore software development.

The KDE Slimbook comes with up to 64 GBs of DDR4 RAM in two memory sockets, and has three USB ports, a USB-C port, an HDMI socket, a RJ45 for wired network connections, as well as support for the new Wifi 6 standard. It also comes in two sizes: the 14-inch screen version weighing 1.07 kg, and the 15.6-inch version weighing 1.49 kg. The screens themselves are Full HD IPS LED and cover 100% the sRGB range, making colors more accurate and life-like, something that designers and photographers will appreciate.

Despite its slim shell, the AMD processor and Plasma software deliver enough power to allow you to deploy a full home office with all the productivity and communications software you need. You can also comfortably browse the web and manage social media, play games, watch videos and listen to music. If you are the creative type, the Ryzen 4800 H CPU is well-equipped to let you express your artistic self, be it with painting apps like Krita, 3D design programs like Blender and FreeCAD, or video-editing software like Kdenlive.

If you are into software development, you are in luck too: KDE provides all the tools you need to code and supports your favorite languages and environments. Meanwhile, Slimbook's hardware is ideal for CPU-intensive tasks and will substantially shorten your build times.

But all this power does not come at an extra cost. The KDE Slimbook starts at approximately € 900, making it more affordable than most similarly-powered laptops.

Besides, the Slimbook company actively supports and sponsors KDE and donates part of the proceedings back into the Community.
