---
title: 'KDE e.V. is looking for a web designer (Hugo) for environmental sustainability project'
date: 2021-10-04 13:30:00
layout: post
noquote: true
---

> Edit 2021-11-16: applications for this position **are closed**.

KDE e.V., the non-profit organisation supporting the KDE community, is looking for a web designer to implement the environmental sustainability project (KDE Eco) website with Hugo.
