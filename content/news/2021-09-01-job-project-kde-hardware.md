---
title: 'KDE e.V. is looking for a developer to help further hardware integration projects'
date: 2021-09-01 20:00:00
layout: post
noquote: true
---

> Edit 2021-10-01: applications for this position **are closed**.

KDE e.V., the non-profit organisation supporting the KDE community, is looking for a someone to help shape products based on KDE software.

We are looking for people who can start working on the projects soon, we expect this to be a part-time position.
Please see the [call for proposals for the KDE Hardware integration project]({{ '/resources/callforproposals-hardware2021.pdf' | prepend: site.url }}) for more details about this contracting opportunity.

Looking forward to hearing from you.
