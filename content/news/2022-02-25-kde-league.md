---
title: 'KDE e.V. to wind down the KDE League'
date: 2022-02-25 1:30:00
layout: post
noquote: true
---

KDE e.V. makes it known that the KDE League is to be wound down and dissolved.
Remaining assets of the League are to be transferred to KDE e.V. Interested
parties are invited to comment.

KDE e.V., the legal entity representing the KDE community, and KDE League, Inc. (the “League”) 
are about to enter into an agreement to wind down and dissolve the League and transfer all of its 
remaining liquid assets (after paying the costs of dissolution) to KDE e.V. 

The League was formed by the KDE community in 2000 as a not-for-profit independent organization 
for the promotion of KDE in the United States.  It represented a joint effort among KDE e.V. and 
numerous companies operating in the KDE ecosphere.  Sadly, the organization has now been inactive 
for over fifteen years, without engaging in any fund-raising nor promotional activities, 
having been unable to obtain quorum for meetings of its board or membership.
The organization retains some unspent funds collected during its early years.
Thanks to renewed efforts by Andreas Pour, its former Chair, Chris Schlager, 
its President, and the board of KDE e.V., the League and KDE e.V. have agreed 
to dissolve the League and transfer remaining liquid assets to KDE e.V. 

This move to dissolve the League is our shared understanding of a “best effort” 
way to satisfy the provisions of the statutes of the League while remaining 
true to the intent of the League members who graciously donated funds to the 
League to promote KDE and the KDE community.

Pursuant to the agreement, subject to resolving any objections brought by any member 
of the League, the League will be dissolved and its remaining assets transferred 
to KDE e.V. approximately 2 months after publication (february 25th, 2022) of this agreement, on april 30th, 2022. 

Any objections to this agreement may be sent to the KDE e.V. board by email (kde-ev board (at) kde.org) or 
by mail or courier to KDE e.V. Prinzenstraße 85 F 10969 Berlin Germany, and by email to the 
League at (KDELeague (at) AdvancedPlacementLaw.com) and must be received by april 28th, 2022.

The full text of the agreement will be made available shortly.

