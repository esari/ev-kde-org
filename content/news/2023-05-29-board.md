---
title: 'KDE e.V. board sprint'
date: 2023-05-29 10:00:00
layout: post
noquote: true
slug: board
categories: [Board]
---

The board of KDE e.V. held an in-person meeting in Berlin, Germany, over the weekend
of 27 and 28 May 2023. Sticky notes for events, HR, and community were
stuck to whiteboards, moved around, and ended up under a label *Done*.
The board sat down with local community members for an Ethiopian dinner
on Saturday evening.
