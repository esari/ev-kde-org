---
title: 'KDE e.V. is looking for a project lead and event manager for environmental sustainability project'
date: 2024-01-14 13:00:00
noquote: true
categories: [Hiring]
---

*Edit 2024-02-08: applications for this position are closed.* KDE e.V. is looking for you as an employee for a new project to promote environmentally-sustainable software and long-term hardware use. You will be the person who makes sure the administration of the project and the organization of the campaigns and workshops are on track and successful. Please see the [full job listing](/resources/jobad-projectlead2024.pdf) for more details about this opportunity.

Applications will be accepted until mid-february, and we look forward to your application.
