---
title: 'Akademy 2023 will be held in Greece'
date: 2023-01-12 10:00:00
layout: post
noquote: true
slug: akademy2023
categories: [Akademy]
author: Adriaan de Groot
---

The yearly conference of the KDE community, which includes
the annual general meeting of KDE e.V., will be held in
Thessaloniki, Greece, from Saturday the 15th to Friday the 21st of July, 2023.
See the [dot story](https://dot.kde.org/2023/01/12/akademy-2023-will-be-held-greece) for more details.
