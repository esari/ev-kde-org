---
title: 'DESKTOP SUMMIT 2011 from 6 to 12 August in Berlin'
date: 2010-10-06 00:00:00 
layout: post
---

<p>The Desktop Summit is a co-located event which features the yearly contributor conferences of the GNOME and KDE communities, GUADEC and Akademy. Next year the conference will take place from 6 to 12 August, 2011 in Berlin at the Humboldt University, Unter den Linden 6. The event will feature keynotes, talks, workshops and team building events. The Desktop Summit website is now <a href="http://www.desktopsummit.org/">online</a>.</p>

<p>The GNOME and KDE communities develop the majority of Free Software desktop technology. Increasingly, they cooperate on underlying infrastructure. By holding their annual developer flagship events in the same location, the two projects will further foster collaboration and discussion between their developer communities. Moreover, KDE and GNOME aim to work more closely with the rest of the desktop and mobile open source community. The summit presents a unique opportunity for main actors to work together and improve the free and open source desktop for all.</p>

<p>"We are proud to be able to welcome the  participants of the Desktop  Summit 2011 to the capital region next  year," says Almuth Nehring-Venus,  Permanent Secretary for Economics, Technology and Women's Issues from the Berlin Senate. "I hope that the joint  conference of the two largest Free  Desktop projects GNOME and KDE will  provide an  additional boost for  our Open Source/Open Standard  initiative within the framework of  Berlin's IT strategy and therefore  also to further  high-value jobs in  our region."</p>

<p>At the Desktop Summit 2011, KDE and GNOME expect well over a thousand core contributors, prominent open source technology leaders, representatives from government, education and corporate backgrounds, and open source desktop engineers, usability experts and designers. Local and international IT industry are invited to learn about and join in developing the latest innovative desktop technology. The TSB Innovation Agency Berlin GmbH and the Berlin Senate are supporting the event locally.</p>

<p>The registration for the Desktop Summit will open on 1 February 2011 and a call for papers will be issued. Talk submissions will be due on 15th of March and on 15 April the conference program will be announced. Saturday  6 August kicks off three days of joint keynotes, talks and social events followed by four days of collaborative workshops. The conference  officially ends on Friday, 12 August with a closing.</p>

<h4>About the Desktop Summit</h4>
<p>The Desktop Summit is a joint initiative by GNOME and KDE to encourage the development and use of a free desktop. The shared objectives include the exchange of technology, cooperation on projects and developing new opportunities with Free and Open Source  Software on the desktop.</p>

<p>The first Desktop Summit in 2009 was a huge success: More than 850 free software advocates from 46 countries gathered together in Las Palmas on Gran Canaria to discuss and enhance the free desktop experience. The summit accomplished its goal of increasing co-operation and throughout the conference there were many examples of improvements to current and development of new shared technologies.</p>

#### About GNOME and the GNOME Foundation

GNOME is a  free-software project whose goal is to develop a complete, accessible and easy to use desktop for Linux and Unix-based operating systems. GNOME also includes a complete development environment to create new applications. It is released twice a year on a regular schedule.

The  GNOME desktop is used by millions of people around the world. GNOME is a  standard part of all leading GNU/Linux and Unix distributions, and is popular with both large existing corporate deployments and millions of  small business and home users worldwide.

Composed of hundreds of volunteer developers and industry-leading companies, the GNOME Foundation is an organization committed to supporting the advancement of  GNOME. The Foundation is a member directed, non-profit organization that provides financial, organizational and legal support to the GNOME project and helps determine its vision and roadmap. More information about GNOME and the GNOME Foundation can be found at <a href="http://www.gnome.org">http://www.gnome.org</a>  and <a href="http://foundation.gnome.org">http://foundation.gnome.org</a>.

#### About KDE and KDE e.V.

KDE is an international community that creates Free Software for desktop and  portable computing. Among KDE's products are innovative workspaces for  Linux and UNIX platforms, the KDE Platform for rapid development and a comprehensive range of applications. KDE offers hundreds of software  titles in many categories including communication and groupware, office  producitvity, web applications, multimedia, entertainment, education,  graphics and software development.

KDE software is translated into more than 60 languages and is built with  ease of use and modern accessibility principles in mind. Applications  built on KDE Platform 4 run natively on Linux, BSD, Solaris, Windows and  Mac OS X.

KDE e.V. is a nonprofit association that represents the international community supported KDE operating legally, financially and organizationally. More informations about KDE and KDE e.V. can be found at <a href="http://www.kde.org">http://www.kde.org</a> and <a href="http://ev.kde.org">http://ev.kde.org</a>.

<hr />

For further informations and interviews please contact us:

<blockquote>
<b>Offical contact Desktop Summit 2011</b>
Mirko Boehhm, Spokesman<br />
c/o KDAB (Deutschland) GmbH &amp; Co. KG<br />
Tempelhofer Ufer 11, D-10963 Berlin<br />
phone +49 (0)30 - 5 21 32 54-70<br />
<a href="http://www.desktopsummit.org">http://www.desktopsummit.org</a><br />

<b>Press contact Desktop Summit (WORLD)</b>
Jos Poortvliet, jos@opensuse.org<br />
mobile +31(0)6 22 37 75 45<br />

<b>Press contact Desktop Summit (DACH)</b>
Thomas Keup, thomas.keup@opencoffee.de<br />
mobile +49(0)171 - 4 18 00 84<br />

<b>Organizer GNOME Foundation</b>
Stormy Peters, Executive Director<br />
e-mail: stormy@gnome.org<br />
<a href="http://foundation.gnome.org/">http://foundation.gnome.org/</a><br />

<b>Organizer KDE e.V.</b>
Claudia Rauch, Business Manager<br />
e-mail rauch@kde.org<br />
<a href="http://ev.kde.org/">http://ev.kde.org/</a><br />
</blockquote>

