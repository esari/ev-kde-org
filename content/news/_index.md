---
title: News
menu:
  main:
    parent: info
    weight: 2
scssFiles:
- /css/list.scss
---

Keep up with the latest news about the KDE e.V.
