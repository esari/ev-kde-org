---
title: 'Call for proposals - Software Platform Engineer'
date: 2022-11-10 20:00:00
layout: post
noquote: true
---

# About KDE
KDE is an international community dedicated to developing Free and Open Source Software for end users. Contributions are made by thousands of talented volunteer contributors (developers, artists, marketers, translators, documentation writers, etc.) worldwide, working on software such as a desktop environment, graphics applications, PIM apps, games, educational software, and more. KDE e.V. is the legal organization behind KDE, representing and supporting the community.

# About the role
KDE's software is built using several toolkits and technologies that are fundamental to bringing great software to our users and enabling a good developer experience for our developer community, and our implementations reach a vast number of platforms and users throughout the world.
You will join KDE‘s vibrant development community to deliver community-driven initiatives that improve the frameworks our software is built upon.

# Qualifications and skills
* 3+ years of experience in KDE or Qt development
* Strong understanding of the underlying technologies of the KDE stack (C++, Qt, KDE Frameworks, preferrably Plasma) and FOSS contribution processes
* Experience developing C++ Frameworks and understanding the challenges it poses
* Ability to work within an open community and gather requirements from different sources
* Deep knowledge of data structures and algorithms

# Essential functions and responsibilities
You will take on tasks that are relevant to KDE projects and need attention to bring our technology forward. This generally means working on KDE Frameworks code, Qt frameworks, and other system services.
* Develop new and existing functionality for Qt, KDE Frameworks, relevant middleware libraries, and other parts of KDE‘s software stack in ways that are of benefit to KDE as a whole
* Prioritize and fix high-priority bugs and regressions in KDE code
* Port KDE code to newer versions of the relevant technology stack, such as Qt 6
* Work with the different KDE teams (Plasma, Frameworks, Goals, the different App teams) to identify future strategic platform milestones and act on them. You will collaborate with KDE members and other open communities on the technical design, work estimation, and implementation of changes and new features.
* Identify and implement changes to generally make KDE Frameworks more attractive and available to developers
* Continually improve software development practices by modernizing code, adding and maintaining unit tests, and performing other maintenance-related tasks

# Job details
* Type: Contract
* Location: Remote
* Salary: Depending on the experience, commensurate with title and responsibilities
* Number of hours per week: 20

# How to apply
Please send your application (including your availability and pointers to the project you are most proud of) by email to kde-ev-board@kde.org with “KDE Software Platform Engineer“ in the subject.
Do not hesitate to contact us at the same address if you have any questions.

