---
title: "General Assembly"
nosubpage: true
menu:
  main:
    parent: activities
    weight: 2
---

> The *General Assembly* or *Annual General Meeting* (AGM) is the yearly meeting where members of the association come together to discuss matters related to the organisation and perform their duties such as electing board members and discharge the existing board.

## AGM 2023

> **Note** the General Assembly 2023 has been completed.

 - **Time:** The next general assembly of KDE e.V. takes place on [Saturday 9th of September at 1 pm UTC](https://zonestamp.toolforge.org/1694264439)
 - **Place:** The general assembly will be held online and in-person at KDE e.V.'s office in Berlin (extremely limited capacity; please register with the board in advance if you want to be there in person).
 - **Agenda:** See the [agenda](agenda) for what is planned to happen at the general assembly.


## Attending the AGM

All members of KDE e.V. are invited to attend the general assembly.
The AGM can be held in person, in hybrid form, or entirely online.
The general assembly of 2023 voted that future AGMs can be called as virtual meetings
where the members taking part must execute their other membership rights via means of electronic communication without being present at the meeting location.
A hybrid or online AGM does not preclude participating by proxy as described below.

If you are not able to participate in the general assembly in person you can ask
another member to act as proxy for you to execute your voting right.
This is regulated by [section 6.6](/corporate/statutes/#6) of the articles of association of the KDE e.V.
Fill out the [proxy form](../resources/proxy_instructions.pdf), sign it, and make sure that it
is available to the chairperson
of the general assembly on the day of the assembly.
Each active member can act as proxy for up to two other active members.

Please print out the proxy form, fill it in, then scan it.
This should then be emailed to your proxy and the <a href="mailto:kde-ev-board@kde.org">board</a>.

If you do not execute your voting rights personally or through a proxy for
two consecutive general assemblies your membership status will be changed from
active to extraordinary, according to [section 4.2](/corporate/statutes/#4) of the articles of association.
Extraordinary and supporting members have the right to attend the general assembly,
but they have no voting rights.

The general assembly is usually held at [Akademy](https://akademy.kde.org). Don't miss the opportunity to be part of this exciting event.

## Activities at the AGM

The AGM is the highest decision-making organ of the association.
The agenda of the AGM is published before the meeting, with all of the scheduled discussions and decisions spelled out.
There are a number of standard elements in the agenda, which are mandated by law:
 - **Election** of a chairperson to run the meeting,
 - **Appointment** of a notes-taker to produce the protocol (minutes) of the meeting,
 - **Report** of the board describing the activities of the association,
 - **Report** of the treasurer describing the financial state of the association,
 - **Report** of the auditors of accounting describing the financial audit of the association,
 - **Vote** on relief of the board.

The financial side of the meeting is an important one. The board is responsible
for the financial outcome of the association each year between the AGMs.
This responsibility is a *burden* on the board.

The auditors of accounting
check the accounting -- the income and expenses of the association, how
spending is justified, etc. -- and deliver a report stating what they find.
Hopefully, the association is carefully managed and the books are complete,
detailed, and fair. The spending satisfies the statutes of the association.

When the accounting is satisfactory, the auditors of accounting recommend to
*relieve* the board of the burden of responsibility: the association's members
as a whole decide that the responsibility now rests with the association, not
with the people who are on the board.

A vote to relieve the board says "yes, the board has behaved responsibly."

## Protocol of the AGM

The official notes of the AGM are taken by the AGM-note-taker and
deposited (in German) with KDE e.V. and the authorities.
Some years have English translations. You can find the
notes of past AGMs on the [reports page](/reports/#meetings).
