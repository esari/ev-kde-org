---
title: 2019 KDE e.V. Report
summary: KDE e.V. is happy to inform you that we have just published the latest report about the activities of our association in 2019. KDE's yearly report gives a comprehensive overview of all that has happened during 2019. It covers the progress we have made with KDE's Plasma and applications as well as results from community sprints, conferences, and external events the KDE community has participated in worldwide.

layout: report

date: 2020-09-05
year: 2019
issue_number: 36

aliases:
  - /reports/ev-2019

parts:
  - title: Home
    anchor: home
    home: true
  - title: Welcome
    heading: Welcome to KDE's Annual Report 2019
    image: images/logos/kde-logo.png
    author: Aleix Pol
    include: 00_welcome-message.html
  - title: Featured article
    heading: Featured article - What is the KDE Free Qt Foundation and What does it do
    image: images/logos/kde-logo.png
    author: Paul Brown
    include: 00_featured-article.html
  - title: Supported Activities
    items:
      - title: Developer Sprints and conferences
        heading: Supported Activities &#8210; Developer Sprints and Conferences
        items:
          - include: 01_plasma_mobile.html
            author: By the Plasma Mobile Team
            heading: Plasma Mobile Sprint
            image: images/logos/kdeconnect.png
          - include: 01_privacy.html
            author: By the Privacy Team
            heading: Privacy Goal Sprint
            image: images/logos/goals.png
          - include: 01_plasma.md
            author: By the Plasma Team
            heading: Plasma Sprint
            image: images/logos/plasma.png
          - include: 01_usability_and_productivity.html
            heading: Usability & Productivity Sprint
            author: By David Redondo and Nicolas Fella
            image: images/logos/goals.png
          - include: 01_onboarding_sprint.md
            author: By Neofytos Kolokotronis 
            heading: Onboarding Sprint
            image: images/logos/goals.png
          - include: 01_kdeconnect.html
            heading: KDE Connect Sprint
            image: images/logos/kdeconnect.png
            author: By Simon Redman and Matthijs Tijink
          - include: 01_krita.html
            image: images/logos/krita.png
            author: By the Krita Team
            heading: The Krita Sprint
          - include: 01_gsoc.html
            heading: Google Summer of Code
            image: images/logos/gsoc_logo.png
            author: By Valorie Zimmerman
          - include: 01_akademy.html
            heading: Akademy 2019
            author: By Paul Brown and Ivana Devcic
            image: images/logos/akademy_logo.png
          - include: 01_las.md
            author: Compiled from blogs by Akhil Gangadharan, Amit Sagtani, Jonathan Riddel, and Kenny Coyle
            heading: Linux App Summit
            image: images/logos/kde-logo.png
          - include: 01_lakademy.md
            author: Compiled from the blogs of Aracele Torres, Caio Tonetti, Filipe Saraiva and Sandro Andrade
            heading: LaKademy
            image: images/logos/lakademy2019.png
          - include: 01_kf6.html
            author: Compiled from notes made by Christoph Cullmann, David Redondo, Kai Uwe, Kevin Ottens, Marco Martin, Nicolas Fella and Volker Krause
            heading: KF6 Sprint
            image: images/logos/kde-logo.png
        
      - title: Trade Shows and Community Events
        heading: Supported Activities &#8210; Trade Shows and Community Events
        items:
          - include: 02_fosdem.html
            heading: FOSDEM
            author: By Paul Brown
            image: images/logos/FOSDEMx47.png
          - include: 02_gitlab.html
            heading: GitLab Commit
            author: By Paul Brown
            image: images/logos/gitlabx47.png
          - include: 02_qtws.html
            heading: Qt World Summit
            author: By Kai Uwe
            image: images/logos/qt.png
          - include: 02_pycon_india.html
            author: By Piyush Aggarwal
            heading: PyCon India
            image: images/logos/pycon.png
          - include: 02_open_expo.md
            heading: OpenExpo Europe 2019
            author: Paul Brown
            image: images/logos/openexpo.png

  - title: Reports
    items:
      - title: Goals
        heading: Goals
        items:
          - heading: It's All about the Apps
            author: By Jonathan Riddell
            include: 03_goal_allabouttheapps.html
            image: images/logos/goals.png
            bglight: true
          - heading: Wayland
            author: By David Edmundson, Aleix Pol and Méven Car
            image: images/logos/goals.png
            include: 03_goal_wayland.html 
            bglight: true
          - heading: Consistency
            author: By Niccolo Venerandi
            image: images/logos/goals.png
            include: 03_goal_consistency.html
            bglight: true
      - title: Working Groups
        heading: Working Groups
        items:
          - heading: Sysadmin
            include: 04_sysadmin.html
            image: images/logos/kde-logo.png
            author: By Ben Cooksley
          - heading: Financial Working Group
            include: 04_fiwg_report.html
            image: images/logos/kde-logo.png
            author: By Eike Hein and David Edmundson
  - title: Community Highlights
    heading: Highlights
    include: 05_highlights.md
    author: the Promo team
  - title: Thoughts from Partners
    heading: Thoughts from Partners    
    include: 05_quotes.html
    author: ""
    nonav: true
  - title: New Members
    heading: New Members    
    include: 05_new-members.html
    author: ""
    nonav: true
  - title: KDE e.V. Board of Directors
    heading: KDE e.V. Board of Directors    
    include: 05_board.html
    author: ""
    nonav: true
  - title: Partners
    heading: Partners
    include: 05_partners.md
    author: ""
    nonav: true

---

<section id="about-kdeev" class="bg-light">
  <div class="container">
    <h2>About KDE e.V.</h2>
    <p><a href="https://ev.kde.org/" target="_blank">KDE e.V.</a> is a registered non-profit organization that represents the <a href="http://www.kde.org" target="_blank">KDE Community</a> in legal and financial matters. The KDE e.V.'s purpose is the promotion and distribution of free desktop software in terms of free software, and the program package "K Desktop Environment (KDE)" in particular, to promote the free exchange of knowledge and equality of opportunity in accessing software as well as education, science and research.</p>
    <ul class="address hidden">
      <li><i class="fa fa-map-marker"></i> <span> Address:</span> Prinzenstraße 85 F,10969 Berlin, Germany </li>
      <li><i class="fa fa-phone"></i> <span> Phone:</span> +49 30 202373050 </li>
      <li><i class="fa fa-envelope"></i> <span> Email:</span><a href="mailto:kde-ev-board@kde.org"> kde-ev-board@kde.org</a></li>
      <li><i class="fa fa-globe"></i> <span> Website:</span> <a href="#">ev.kde.org</a></li>
    </ul>
  </div>
</section>
<section class="section">
  <div class="container">
    <p>Report prepared by Paul Brown with the help of Carl Schwan, Aniqa Khokhar, Aleix Pol, Neophytos Kolokotronis, Ben Cooksley, Niccolo Venerandi, David Edmundson, Méven Car, Jonathan Riddell, Piyush Aggarwal, Kai Uwe, Christoph Cullmann, David Redondo, Kevin Ottens, Marco Martin, Nicolas Fella, Volker Krause, Aracele Torres, Caio Tonetti, Filipe Saraiva, Sandro Andrade, Akhil Gangadharan, Amit Sagtani, Kenny Coyle, Ivana Devcic, Valorie Zimmerman, Simon Redman, Matthijs Tijink, the Krita Team, the Plasma Team, the Privacy Team, the Plasma Mobile Team and the Promo Team at large.</p>
    <p>This report is published by KDE e.V., copyright 2020, and licensed under <a href="https://creativecommons.org/licenses/by/3.0/">Creative Commons-BY-3.0</a>.</p>
    <a href="https://ev.kde.org/" target="_blank" class="text-center">
      <img class="img-responsive" src="/reports/images/logo.png" alt=""/>
    </a>
   </div>
</section>
